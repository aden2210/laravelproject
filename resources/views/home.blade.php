@extends('biodata.index')
@section('container')
        <header class="bgmaster">
            <div class="container px-4 px-lg-5 d-flex h-100 align-items-center justify-content-center">
                <div class="d-flex justify-content-center">
                    <div class="text-center">
                        <a class="btn btn-primary" href="/about">TENTANG SAYA</a>
                    </div>
                </div>
            </div>
        </header>
@endsection