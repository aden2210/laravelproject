@extends('biodata.index')
@section('container')
<section class="about-section text-center" id="about">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-lg-8">
                        <h2 class="text-white mb-4">I Made Ary Dhana Iswara</h2>
                        <img class="img-fluid pb-4" src="{{asset('template/assets/img/ProfilAden.jpg')}}" alt="..." width="500" />
                        <p class="text-white-50 mb-4">
                            Hallo kawan-kawan, perkenalkan namaku I Made Ary Dhana Iswara, bisa dipanggil ary ataupun aden. Aku merupakan mahasiswa di prodi Sistem Informasi, Fakultas Teknik dan Kejuruan, Universitas Pendidikan Ganesha. Aku berada pada angkatan 2019 Sistem Informasi.
                        </p>
                        <p class="text-white-50 mb-4">
                        Hoby saya yaitu game dan musik. Hoby tersebut membantuku saat sedang stress dalam pembuatan project.
                        </p>
                        <p class="text-white-50 mb-4">
                        Pengalamanku dalam membuat aplikasi belum terbilang banyak. Hanya mencoba-coba membuat aplikasi mobile menggunakan fluter dan juga mencoba membuat versi webnya.
                    </div>
                </div>
            </div>
        </section>
@endsection